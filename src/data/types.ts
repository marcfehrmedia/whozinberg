export interface User {
    id: string,
    email: string,
    updated_at?: string,
    username?: string,
    full_name?: string,
    avatar_url?: string,
    website?: string,
}