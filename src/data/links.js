export const NAVBAR_LINKS = [
    {
        title: 'Log in',
        icon: '⚡️',
        href: '/user/login',
        visible: ['guest']
    },
    {
        title: 'Register',
        icon: '＋',
        href: '/user/register',
        visible: ['guest']
    },
    {
        title: 'Map',
        icon: '🗺️',
        href: '/map',
        visible: ['admin', 'user']
    },
    {
        title: 'List',
        icon: '📋',
        href: '/list',
        visible: ['admin', 'user']
    },
    {
        title: 'Calendar',
        icon: '🗓️',
        href: '/calendar',
        visible: ['admin', 'user']
    }
]

export const PROFILE_LINKS = [
    {
        title: 'My listings',
        icon: '✨',
        href: '/user/my-listings',
        visible: ['admin', 'user']
    },
    // {
    //     title: 'Your profile',
    //     icon: '',
    //     href: '/profile',
    //     visible: ['admin', 'user']
    // },
    {
        title: 'Settings',
        icon: '⚙️',
        href: '/user/settings',
        visible: ['admin', 'user']
    },
    {
        title: 'Log out',
        icon: '✌️',
        href: '/api/auth/sign-out',
        visible: ['admin', 'user']
    }
]