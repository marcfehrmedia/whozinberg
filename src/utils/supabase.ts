import { createClient } from '@supabase/supabase-js'
import cookie from 'cookie'

const supabaseUrl = import.meta.env.PUBLIC_SUPABASE_URL
const supabaseAnonKey = import.meta.env.PUBLIC_SUPABASE_KEY

export const supabase = createClient(supabaseUrl, supabaseAnonKey)

export async function getUser(req: Request) {
    const c = cookie.parse(req.headers.get('cookie') ?? '')

    if (!c.sbat) {
        return null
    }

    const {
        data: { user },
    } = await supabase.auth.getUser(c.sbat)
    if (!user || user.role !== 'authenticated') {
        return null
    }

    return user
}