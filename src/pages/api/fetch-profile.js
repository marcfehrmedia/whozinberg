import { supabase } from '../../utils/supabase'

export async function GET({ url }) {
	const identity_id = url.searchParams.get('identity_id');

	try {
		const { data, error } = await supabase
			.from('profiles')
			.select()
			.eq('id', identity_id)
			.single();

		if (error) throw error;

		// console.log(data)

		return new Response(JSON.stringify(data), {
			status: 200,
			headers: {
				'Content-Type': 'application/json'
			}
		});
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}