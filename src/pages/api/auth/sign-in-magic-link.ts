import { supabase } from "../../../utils/supabase";

import type { APIRoute } from "astro";

export const POST: APIRoute = async ({ request, cookies, redirect }) => {
    const body = await request.json();
    const { access_token, refresh_token } = body;

    if (access_token && refresh_token) {
        cookies.set("sb-access-token", access_token, {
            sameSite: "strict",
            path: "/",
            secure: true,
        });

        cookies.set("sb-refresh-token", refresh_token, {
            sameSite: "strict",
            path: "/",
            secure: true,
        });

        return redirect("/user/settings");
    }
    else {
        return new Response("No access token or refresh token provided", { status: 400 });
    }
};