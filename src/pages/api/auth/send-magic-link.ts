import { supabase } from "../../../utils/supabase";
import type { APIRoute } from "astro";

export const POST: APIRoute = async ({ request }) => {
    try {
        const formData = await request.formData();
        const email = formData.get('email');

        if (!email) {
            return new Response(JSON.stringify({ error: 'Email is required' }), {
                status: 400,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        }

        // Send magic link
        const { error } = await supabase.auth.signInWithOtp({
            email: email.toString(),
            options: {
                emailRedirectTo: `${import.meta.env.PUBLIC_BASE_URL}/user/magic-link-check`
            },
        });

        if (error?.message) {
            return new Response(JSON.stringify({ error: error.message }), {
                status: 400,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        } else {
            return new Response(JSON.stringify({ message: 'Magic link sent successfully' }), {
                status: 200,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        }

    } catch (error) {
        return new Response(JSON.stringify({ error: error.message }), {
            status: 500,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
};
