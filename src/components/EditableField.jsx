import { useState, useEffect } from 'react';

const EditableField = ({ label, value, onSave }) => {
    const [editableValue, setEditableValue] = useState(value);
    const [isEdited, setIsEdited] = useState(false);

    useEffect(() => {
        setIsEdited(editableValue !== value);
    }, [editableValue, value]);

    const handleSave = () => {
        if (isEdited && onSave) {
            onSave(label.toLowerCase(), editableValue);
        }
    };


    return (
        <tr className='border-t border-gray-300'>
            <td className='text-left whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-3'>{label}</td>
            <td className='text-left whitespace-nowrap px-3 py-4 text-sm text-gray-500'>
                <input
                    type='text'
                    value={editableValue}
                    className={'border-t-0 border-l-0 border-r-0 border-b-2 border-zinc-200 w-full focus:border-b-2 focus:outline-none focus:ring-0 focus:border-zinc-200'}
                    onChange={(e) => setEditableValue(e.target.value)}
                />
            </td>
            <td className='relative whitespace-nowrap py-4 pl-3 pr-4 text-left text-sm font-medium sm:pr-3'>
                {isEdited &&
                    <button
                        className={`text-zinc-600 hover:text-blue-500 cursor-pointer`}
                        onClick={handleSave}
                        disabled={!isEdited}
                    >
                        Save
                    </button>
                }
            </td>
        </tr>
    );
}

export default EditableField;
