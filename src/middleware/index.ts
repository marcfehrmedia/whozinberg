import { defineMiddleware } from "astro:middleware";
import { supabase } from "../utils/supabase";
import micromatch from "micromatch";

const protectedRoutes = [
    // "/",
    "/user/settings(|/)",
    "/map(|/)",
    "/user/list(|/)",
    "/user/calendar(|/)"
];

const redirectRoutes = [
    "/user/login(|/)",
    "/user/register(|/)",
    "/user/reset-password(|/)"
];

// Function to check if the user is logged in
export async function isUserLoggedIn(cookies) {
    const accessToken = cookies.get("sb-access-token");
    const refreshToken = cookies.get("sb-refresh-token");

    if (!accessToken || !refreshToken) {
        return false;
    }

    const { data, error } = await supabase.auth.setSession({
        refresh_token: refreshToken.value,
        access_token: accessToken.value,
    });

    if (error) {
        return false;
    }

    return data.session != null;
}

export const onRequest = defineMiddleware(
    async ({ locals, url, cookies, redirect }, next) => {
        if (micromatch.isMatch(url.pathname, protectedRoutes)) {
            const accessToken = cookies.get("sb-access-token");
            const refreshToken = cookies.get("sb-refresh-token");

            if (!accessToken || !refreshToken) {
                return redirect("/user/login");
            }

            const { data, error } = await supabase.auth.setSession({
                refresh_token: refreshToken.value,
                access_token: accessToken.value,
            });

            if (error) {
                cookies.delete("sb-access-token", {
                    path: "/",
                });
                cookies.delete("sb-refresh-token", {
                    path: "/",
                });
                return redirect("/user/login");
            }

            // interface Locals {
            //     email?: string;
            // }

            // @ts-ignore: Type definitions for locals are missing
            locals.email = data.user?.email!;

            // @ts-ignore: Type definitions for locals are missing
            locals.user = data.user

            // Set isLoggedIn to true if the user is logged in
            const userIsAuthenticated = await isUserLoggedIn(cookies);

            // @ts-ignore: Type definitions for locals are missing
            locals.isLoggedIn = userIsAuthenticated;

            cookies.set("sb-access-token", data?.session?.access_token!, {
                sameSite: "strict",
                path: "/",
                secure: true,
            });

            cookies.set("sb-refresh-token", data?.session?.refresh_token!, {
                sameSite: "strict",
                path: "/",
                secure: true,
            });
        }

        // Redirect to settings if the user is logged in and tries to access the login or register page
        if (micromatch.isMatch(url.pathname, redirectRoutes)) {
            const accessToken = cookies.get("sb-access-token");
            const refreshToken = cookies.get("sb-refresh-token");

            if (accessToken && refreshToken) {
                return redirect("/user/settings");
            }
        }
        return next();
    },
);